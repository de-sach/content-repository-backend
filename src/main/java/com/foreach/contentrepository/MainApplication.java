package com.foreach.contentrepository;

import com.foreach.across.config.AcrossApplication;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.applicationinfo.ApplicationInfoModule;
import com.foreach.across.modules.contentrepository.ContentRepositoryModule;
import com.foreach.across.modules.contentrepository.ContentRepositoryModuleSettings;
import com.foreach.across.modules.debugweb.DebugWebModule;
import com.foreach.across.modules.entity.EntityModule;
import com.foreach.across.modules.hibernate.jpa.AcrossHibernateJpaModule;
import com.foreach.across.modules.logging.LoggingModule;
import com.foreach.across.modules.spring.security.SpringSecurityModule;
import com.foreach.across.modules.user.UserModule;
import com.foreach.across.modules.web.AcrossWebModule;
import com.foreach.across.modules.oauth2.OAuth2Module;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.h2.H2ConsoleAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

@AcrossApplication(
        modules = {
                AcrossWebModule.NAME,
                AdminWebModule.NAME,
                DebugWebModule.NAME,
                LoggingModule.NAME,
                ApplicationInfoModule.NAME,
                AcrossHibernateJpaModule.NAME,
                UserModule.NAME,
                SpringSecurityModule.NAME,
                EntityModule.NAME,
                OAuth2Module.NAME
        })
@Import({DataSourceAutoConfiguration.class, H2ConsoleAutoConfiguration.class})
public class MainApplication {
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(MainApplication.class);
        springApplication.setDefaultProperties(Collections.singletonMap("spring.config.location", "${user.home}/dev-configs/dec18-application.yml"));
        springApplication.run(args);
    }

    @Bean
    public ContentRepositoryModule contentRepositoryModule() {
        ContentRepositoryModule module = new ContentRepositoryModule();
        module.setProperty(ContentRepositoryModuleSettings.ALLOW_PREAUTHENTICATED, true);
        // gives a "no authentication found on request" when disabled. -> used to
        module.setProperty(ContentRepositoryModuleSettings.DISABLE_DEFAULT_AUTHENTICATOR, false);
        Path path = Paths.get("/home/ec2-user/efs/repository");
        module.setProperty(ContentRepositoryModuleSettings.REPOSITORY_FOLDER, path.toString());
        module.setProperty(ContentRepositoryModuleSettings.ALLOW_FILE_METADATA_UPDATE, true);
        return module;
    }
}
