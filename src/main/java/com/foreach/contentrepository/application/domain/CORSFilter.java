package com.foreach.contentrepository.application.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CORSFilter implements Filter {
    public static final Logger LOGGER = LoggerFactory.getLogger(CORSFilter.class);
//    private Environment environment;
//
//    public CORSFilter( Environment environment ) {
//        this.environment = environment;
//    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        LOGGER.info("handeling cors request");
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (request.getHeader("Origin") != null) {
            if (request.getHeader("Origin").contains("localhost")) {
                response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
                response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
                response.setHeader("Access-Control-Max-Age", "3600");
                response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Content-Type, Authorization, Accept, Content-Type");
                response.setHeader("Access-Control-Allow-Credentials", "true");
                if ("OPTIONS".equals(request.getMethod())) {
                    LOGGER.info("handeling options request");
                    response.resetBuffer();
                    response.setStatus(HttpServletResponse.SC_OK);
                    return;
                }
            }
        }
        filterChain.doFilter(servletRequest, response);
    }

    @Override
    public void destroy() {

    }
}
