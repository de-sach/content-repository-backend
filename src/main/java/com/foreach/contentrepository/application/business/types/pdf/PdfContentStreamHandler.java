package com.foreach.contentrepository.application.business.types.pdf;

import com.foreach.across.modules.contentrepository.business.CdsObjectVersion;
import com.foreach.across.modules.contentrepository.business.CdsRepository;
import com.foreach.across.modules.contentrepository.cmis.ContentStreamHandler;
import com.foreach.across.modules.contentrepository.cmis.MimeTypeHelper;
import com.foreach.across.modules.contentrepository.cmis.types.document.DocumentContentStreamHandler;
import com.foreach.across.modules.contentrepository.dto.CdsObjectStreamDto;
import com.foreach.across.modules.contentrepository.services.CdsFileManager;
import com.foreach.across.modules.contentrepository.services.CdsObjectStreamService;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.exceptions.CmisNameConstraintViolationException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisStorageException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class PdfContentStreamHandler extends DocumentContentStreamHandler implements ContentStreamHandler {

    private CdsObjectStreamService cdsObjectStreamService;
    private CdsFileManager cdsFileManager;
    private final MimeTypeHelper mimeTypeHelper = new MimeTypeHelper();


    public PdfContentStreamHandler(CdsObjectStreamService cdsObjectStreamService, CdsFileManager cdsFileManager) {
        this.cdsObjectStreamService = cdsObjectStreamService;
        this.cdsFileManager = cdsFileManager;
    }

    @Override
    public CdsObjectVersion setContentStream(CdsObjectVersion objectVersion, ContentStream contentStream, String name) {
        CdsRepository repository = objectVersion.getObject().getRepository();

        // Detect the mime-type before it is closed below
        String mimeType = mimeTypeHelper.detectMimeType( contentStream, name );

        CdsObjectStreamDto objectStream = createObjectStreamDto( objectVersion, name );

        // write content, if available
        if ( contentStream != null && contentStream.getStream() != null ) {
            if ( cdsFileManager.exists( repository, objectStream ) ) {
                //TODO: versioning
                throw new CmisNameConstraintViolationException( "Document already exists!" );
            }

            File newFile;
            // create the file
            try {
                newFile = cdsFileManager.createFile( repository, objectStream );
            }
            catch ( IOException e ) {
                throw new CmisStorageException( "Could not create file: " + e.getMessage(), e );
            }

            cdsFileManager.writeContent( newFile, contentStream.getStream() );

            objectStream.setFileSize( FileUtils.sizeOf( newFile ) );
        }

        objectStream.setMimeType( mimeType );

        cdsObjectStreamService.save( objectStream, objectVersion );
        // OWN EDITS

        // END OWN EDITS
        return objectVersion;
    }

    private CdsObjectStreamDto createObjectStreamDto( CdsObjectVersion objectVersion, String name ) {
        CdsObjectStreamDto objectStream = new CdsObjectStreamDto();
        objectStream.setStreamUuid( UUID.randomUUID().toString() );
        objectStream.setFileName( name );
        objectStream.setFilePath( cdsFileManager.generatePath() );
        objectStream.setMimeType( "unknown" );
        objectStream.setObject( objectVersion.getObject() );
        return objectStream;
    }

}
