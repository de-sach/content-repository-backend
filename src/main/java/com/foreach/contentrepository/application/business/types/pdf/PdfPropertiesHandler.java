package com.foreach.contentrepository.application.business.types.pdf;

import com.foreach.across.modules.contentrepository.business.CdsObject;
import com.foreach.across.modules.contentrepository.business.CdsObjectStream;
import com.foreach.across.modules.contentrepository.business.CdsObjectVersion;
import com.foreach.across.modules.contentrepository.business.CdsRepository;
import com.foreach.across.modules.contentrepository.cmis.PropertiesHandler;
import com.foreach.across.modules.contentrepository.cmis.builders.PropertiesWrapper;
import com.foreach.across.modules.contentrepository.services.CdsFileManager;
import com.foreach.across.modules.contentrepository.services.CdsObjectStreamService;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.impl.server.ObjectInfoImpl;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PdfPropertiesHandler implements PropertiesHandler {
    public static final Logger LOGGER = LoggerFactory.getLogger(PdfPropertiesHandler.class);


    private CdsFileManager cdsFileManager;
    private CdsObjectStreamService cdsObjectStreamService;
    private ApiConnector apiConnector;
    private List<String> processedItems;
    private Map<String, List<String>> mappedMetadata;

    public PdfPropertiesHandler(CdsObjectStreamService cdsObjectStreamService, CdsFileManager cdsFileManager) {
        this.cdsObjectStreamService = cdsObjectStreamService;
        this.cdsFileManager = cdsFileManager;
        this.processedItems = new ArrayList<>();
        this.mappedMetadata = new HashMap<>();
    }

    @Override
    public void compileProperties(CdsObjectVersion objectVersion, ObjectInfoImpl objectInfo, PropertiesWrapper properties) {
        CdsObject object = objectVersion.getObject();

        objectInfo.setHasAcl(true);
        objectInfo.setHasContent(true);
        objectInfo.setHasParent(object.getParent() != null);
        objectInfo.setRelationshipSourceIds(null);
        objectInfo.setRelationshipTargetIds(null);
        objectInfo.setRenditionInfos(null);
        objectInfo.setSupportsDescendants(false);
        objectInfo.setSupportsFolderTree(false);
        objectInfo.setSupportsPolicies(false);
        objectInfo.setSupportsRelationships(true);

        CdsObjectStream cdsObjectStream = cdsObjectStreamService.getObjectStreamForVersion(objectVersion);

        if (cdsObjectStream == null) {
            properties.addId(PropertyIds.CONTENT_STREAM_ID, null);
            properties.addBigInteger(PropertyIds.CONTENT_STREAM_LENGTH, null);
            properties.addString(PropertyIds.CONTENT_STREAM_MIME_TYPE, null);
            properties.addString(PropertyIds.CONTENT_STREAM_FILE_NAME, null);

            objectInfo.setHasContent(false);
            objectInfo.setContentType(null);
            objectInfo.setFileName(null);
        } else {
            properties.addId(PropertyIds.CONTENT_STREAM_ID, cdsObjectStream.getStreamUuid());
            properties.addInteger(PropertyIds.CONTENT_STREAM_LENGTH, cdsObjectStream.getFileSize());
            properties.addString(PropertyIds.CONTENT_STREAM_MIME_TYPE, cdsObjectStream.getMimeType());
            properties.addString(PropertyIds.CONTENT_STREAM_FILE_NAME, cdsObjectStream.getFileName());

            objectInfo.setHasContent(true);
            objectInfo.setContentType(cdsObjectStream.getMimeType());
            objectInfo.setFileName(cdsObjectStream.getFileName());
        }

        // own testing code
        if (properties.getString(PropertyIds.DESCRIPTION) != null) {
            if (properties.getString(PropertyIds.DESCRIPTION).equals("[pdf, document]")) {
//            LOGGER.info("adding metadata to file description");
                // PDF TEXT EXTRACTION
                CdsRepository repository = object.getRepository();
                CdsObjectStream objectStreamForVersion = cdsObjectStreamService.getObjectStreamForVersion(objectVersion);
                File file = cdsFileManager.getFile(repository, objectStreamForVersion);
                if (!processedItems.contains(file.getName())) {
                    addMetadataForFile(file);
                }
                properties.addString(PropertyIds.DESCRIPTION, mappedMetadata.get(file.getName()).toString());
                // end own testing code
            }
        }
        // TODO: ACL, file properties, content stream properties -> are not in the document at the moment

    }

    private void addMetadataForFile(File file) {
        processedItems.add(file.getName());
        String text = null;
        PDDocument pdDocument;
        try {
            pdDocument = PDDocument.load(file);
            text = new PDFTextStripper().getText(pdDocument);
            pdDocument.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // TEXT TRANSLATION
//        apiConnector = new ApiConnector();
//        String translatedText = apiConnector.translateText(text);
//        LOGGER.info(translatedText);

        // TEXT ANALYSIS
//        List<String> analyzeText = apiConnector.analyzeTextWatson(translatedText);
        List<String> analyzeText = new ArrayList<>();
        analyzeText.add(file.getName());
        LOGGER.info(analyzeText.toString());
        mappedMetadata.put(file.getName(), analyzeText);
    }
}
