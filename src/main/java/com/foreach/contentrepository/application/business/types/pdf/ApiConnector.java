package com.foreach.contentrepository.application.business.types.pdf;

import com.ibm.watson.developer_cloud.natural_language_understanding.v1.NaturalLanguageUnderstanding;
import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class ApiConnector {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiConnector.class);

    private static final int MAX_TRANSLATE_LENGTH = 10000;

    public ApiConnector() {
        LOGGER.info("apiConnector created");
    }

    public String translateText(String text) {
        LOGGER.info("text length: " + text.length());
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String url = "https://api.microsofttranslator.com/V2/Http.svc/Translate";
        String parameters = "?to=en-us&text=";
        String constantString = url + parameters;
        LOGGER.info("constant length: " + constantString.length());
        String[] strings = text.split("\n");
        String[] finalText = new String[10];
        String translation = "";
        int count = 0;
        finalText[count] = "";
        for (String line : strings) {
            try {
                line = URLEncoder.encode(line, "UTF-8");
                if ((finalText[count].length() + line.length()) > (MAX_TRANSLATE_LENGTH - constantString.length())) {
                    count++;
                    LOGGER.info("string length: " + finalText[count - 1].length());
                    if (count > 9) {
                        break;
                    }
                    finalText[count] = "";
                }
                finalText[count] += line;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        try {
            for (int i = 0; i < count + 1; i++) {
                String realUrl = url;
                String realParams = parameters;
                realParams += finalText[i];
                realUrl += realParams;
                URIBuilder builder = new URIBuilder(realUrl);
                URI uri = builder.build();
                LOGGER.info("url length: " + uri.toString().length());
                HttpGet request = new HttpGet(uri);
                request.setHeader("Content-Type", "application/json");
                request.setHeader("Ocp-Apim-Subscription-Key", "e33cf35c33784a6fb3b8d6a537f44858");
                request.setHeader("Accept", "application/json");
                HttpResponse response = httpClient.execute(request);
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    InputStream inputStream = entity.getContent();
                    StringWriter stringWriter = new StringWriter();
                    IOUtils.copy(inputStream, stringWriter, "UTF-8");
                    String responseString = stringWriter.toString();
                    responseString = this.cleanResponseString(responseString);
                    translation += responseString;
                }
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return translation;
    }


    public List<String> analyzeTextWatson(String text) {
        List<String> keywords = new ArrayList<>();
        NaturalLanguageUnderstanding service = new NaturalLanguageUnderstanding(
                "2018-03-16",
                "a323142f-237c-4df4-9b41-11c7f607724a",
                "xKFZYRUY7u7L"
        );

        EntitiesOptions entitiesOptions = new EntitiesOptions.Builder()
                .emotion(false)
                .sentiment(false)
                .limit(10)
                .build();

        KeywordsOptions keywordsOptions = new KeywordsOptions.Builder()
                .emotion(false)
                .sentiment(false)
                .limit(30)
                .build();

        Features features = new Features.Builder()
                .entities(entitiesOptions)
                .keywords(keywordsOptions)
                .build();

        AnalyzeOptions parameters = new AnalyzeOptions.Builder()
                .text(text)
                .features(features)
                .build();

        AnalysisResults response = service
                .analyze(parameters)
                .execute();

        LOGGER.info("got response");
        List<KeywordsResult> responseKeywords = response.getKeywords();
        List<EntitiesResult> responseEntities = response.getEntities();
        for (KeywordsResult keywordsResult : responseKeywords) {
            LOGGER.info(keywordsResult.getText());
            if (keywordsResult.getRelevance() > 0.8) {
                keywords.add(keywordsResult.getText());
            }
        }
        for (EntitiesResult entitiesResult : responseEntities) {
            LOGGER.info(entitiesResult.getText());
            if (entitiesResult.getRelevance() > 0.9) {
                keywords.add(entitiesResult.getText());
            }
        }

        return keywords;
    }

    private String cleanResponseString(String responseString) {
        String[] split = responseString.split("<");
        responseString = split[1].split(">")[1];
        return responseString;
    }
}
