package com.foreach.contentrepository.application.installers;

import com.foreach.across.modules.oauth2.business.OAuth2Client;
import com.foreach.across.modules.oauth2.business.OAuth2ClientScope;
import com.foreach.across.modules.oauth2.business.OAuth2Scope;
import com.foreach.across.modules.oauth2.services.OAuth2Service;
import com.foreach.across.modules.spring.security.acl.business.AclPermission;
import com.foreach.across.modules.spring.security.acl.business.AclSecurityEntity;
import com.foreach.across.modules.spring.security.acl.repositories.AclSecurityEntityRepository;
import com.foreach.across.modules.spring.security.acl.services.AclSecurityEntityService;
import com.foreach.across.modules.spring.security.acl.services.AclSecurityService;
import com.foreach.across.modules.spring.security.infrastructure.services.SecurityPrincipalService;
import com.foreach.across.modules.user.business.Role;
import com.foreach.across.modules.user.services.MachinePrincipalService;
import com.foreach.across.modules.user.services.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public abstract class BaseClientInstaller {

    public static final Logger LOGGER = LoggerFactory.getLogger(BaseClientInstaller.class);
    protected static final String ROLE_CLIENT_UNTRUSTED = "ROLE_CLIENT_UNTRUSTED";
    private static final int YEAR = (int) TimeUnit.DAYS.toSeconds(365);

    protected SecurityPrincipalService securityPrincipalService;
    protected MachinePrincipalService machinePrincipalService;
    protected AclSecurityService aclSecurityService;
    protected RoleService roleService;
    protected OAuth2Service oAuth2Service;

    @Autowired
    private AclSecurityEntityRepository aclSecurityEntityRepository;

    @Autowired
    private AclSecurityEntityService aclSecurityEntityService;

    protected OAuth2Client createClient(String clientId, String clientSecret, String role, Collection<String> resourceIds) {
        // First create role if it doesn't exists
        if (roleService.getRole(role) == null) {
            switch (role) {
                case ROLE_CLIENT_UNTRUSTED:
                    roleService.defineRole(role, "Regular - untrusted OAuth2 client", "Regular - untrusted OAuth2 client", Collections.emptySet());
                    break;
                case "ROLE_CLIENT_FORMULA":
                    Collection<String> formulaPermissions = new ArrayList<>();
                    formulaPermissions.add("access administration");
                    roleService.defineRole(role, "Formula User Client", "Formula User Client", formulaPermissions);
                    break;
                default:
                    break;
            }
        }
        Collection<Role> roleServiceRoles = roleService.getRoles();
        roleServiceRoles.size();

        OAuth2Client client = getClientById(clientId).orElseGet(() -> {
            Collection<Role> roles = new HashSet<>();
            roles.add(roleService.getRole(role));

            OAuth2Client dto = new OAuth2Client();
            dto.setClientId(clientId);
            dto.setClientSecret(clientSecret);
            dto.setSecretRequired(true);
            dto.getRoles().addAll(roles);

            linkFullScope(dto);
            dto.setAccessTokenValiditySeconds(YEAR * 10);
            dto.setRefreshTokenValiditySeconds(YEAR * 10);
            return oAuth2Service.saveClient(dto);
        });

        Role formulaServiceRole = roleService.getRole(role);
        aclSecurityService.allow(client, formulaServiceRole, AclPermission.CREATE, AclPermission.WRITE, AclPermission.READ, AclPermission.DELETE);
        client.getRoles().addAll(client.getRoles().stream().filter(role1 -> !role1.getName().equals(role)).collect(Collectors.toList()));
        client.getResourceIds().addAll(resourceIds.stream().filter(r -> !client.getResourceIds().contains(r)).collect(Collectors.toList()));
        client.getAuthorizedGrantTypes().addAll(getTrustedGrantTypes().stream().filter(r -> !client.getAuthorizedGrantTypes().contains(r)).collect(Collectors.toList()));
        return oAuth2Service.saveClient(client);
    }

    private Optional<OAuth2Client> getClientById(String clientId) {
        return Optional.ofNullable(oAuth2Service.getClientById(clientId));
    }

    private Collection<String> getTrustedGrantTypes() {
        Collection<String> trustedGrantTypes = new HashSet<>();
        trustedGrantTypes.add("client_credentials");
        trustedGrantTypes.add("password");
        trustedGrantTypes.add("refresh_token");
        trustedGrantTypes.add("social");
        return trustedGrantTypes;
    }

    private void linkFullScope(OAuth2Client client) {
        OAuth2Scope fullScope = getFullScope();
        OAuth2ClientScope clientScope = new OAuth2ClientScope();
        clientScope.setAutoApprove(false);
        clientScope.setOAuth2Scope(fullScope);
        clientScope.setOAuth2Client(client);
        client.setOAuth2ClientScopes(Collections.singleton(clientScope));
    }

    private OAuth2Scope getFullScope() {
        Iterable<OAuth2Scope> scopes = oAuth2Service.getOAuth2Scopes();
        OAuth2Scope fullScope = null;
        for (OAuth2Scope scope : scopes) {
            if ("full".equals(scope.getName())) {
                fullScope = scope;
            }
        }
        if (fullScope == null) {
            fullScope = new OAuth2Scope();
            fullScope.setName("full");

            oAuth2Service.saveScope(fullScope);
        }
        return fullScope;
    }

    @Autowired
    public void setAclSecurityService(AclSecurityService aclSecurityService) {
        this.aclSecurityService = aclSecurityService;
    }

    @Autowired
    public void setMachinePrincipalService(MachinePrincipalService machinePrincipalService) {
        this.machinePrincipalService = machinePrincipalService;
    }

    @Autowired
    public void setOAuth2Service(OAuth2Service oAuth2Service) {
        this.oAuth2Service = oAuth2Service;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setSecurityPrincipalService(SecurityPrincipalService securityPrincipalService) {
        this.securityPrincipalService = securityPrincipalService;
    }

}
