package com.foreach.contentrepository.application.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.core.installers.InstallerRunCondition;
import com.foreach.across.modules.spring.security.infrastructure.services.CloseableAuthentication;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Installer(description = "ForeachClientInstaller", phase = InstallerPhase.AfterModuleBootstrap, runCondition = InstallerRunCondition.AlwaysRun)
public class ForeachClientInstaller extends BaseClientInstaller {

    @InstallerMethod
    public void installClients() {
        try (CloseableAuthentication ignore = securityPrincipalService.authenticate(machinePrincipalService.getMachinePrincipalByName("system"))) {
            List<String> resourceIds = Collections.singletonList("todo");
            List<String> formulaResourceIds = Collections.singletonList("formula");
            createClient("foreach", "kkjr6uMQPYULvq85", ROLE_CLIENT_UNTRUSTED, resourceIds);
            createClient("formula", "RHVSR4fTWTScB5Pz", "ROLE_CLIENT_FORMULA", formulaResourceIds);
        }
    }

}
