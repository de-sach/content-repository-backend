package com.foreach.contentrepository.application.extensions.contentRepositoryModuleConfiguration;

import com.foreach.across.core.annotations.ModuleConfiguration;
import com.foreach.across.modules.contentrepository.ContentRepositoryModule;
import com.foreach.across.modules.contentrepository.cmis.CdsCmisTypeManager;
import com.foreach.across.modules.contentrepository.cmis.ContentStreamHandler;
import com.foreach.across.modules.contentrepository.cmis.PropertiesHandler;
import com.foreach.across.modules.contentrepository.cmis.StrategyHandlerRegistry;
import com.foreach.across.modules.contentrepository.services.CdsFileManager;
import com.foreach.across.modules.contentrepository.services.CdsObjectStreamService;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Arrays;

//import com.foreach.across.modules.contentrepository.config.types.MetadataTypeId;

@ModuleConfiguration(ContentRepositoryModule.NAME)
public class ContentRepositoryModuleConfiguration {
    public static final Logger LOGGER = LoggerFactory.getLogger(ContentRepositoryModuleConfiguration.class);

    @Autowired
    private CdsCmisTypeManager typeManager;

    @Autowired
    private StrategyHandlerRegistry<PropertiesHandler> propertiesHandlers;

    @Autowired
    private CdsObjectStreamService cdsObjectStreamService;

    @Autowired
    private StrategyHandlerRegistry<ContentStreamHandler> contentStreamHandlers;

    @Autowired
    private CdsFileManager cdsFileManager;

    @PostConstruct
    public void register() {
        //typemanager is not null

        MetadataTypeConfigurer metadataTypeConfigurer = new MetadataTypeConfigurer();
        metadataTypeConfigurer.registerType(typeManager);

        PdfTypeConfigurer pdfTypeConfigurer = new PdfTypeConfigurer();
        pdfTypeConfigurer.registerType(typeManager, propertiesHandlers, contentStreamHandlers, cdsObjectStreamService, cdsFileManager);

        typeManager.registerFilingRestrictions(
                BaseTypeId.CMIS_FOLDER.value(),
                Arrays.asList(
                        BaseTypeId.CMIS_DOCUMENT.value(),
                        BaseTypeId.CMIS_FOLDER.value(),
                        MetadataTypeId.TYPE_DOCUMENT,
                        PdfTypeId.TYPE_DOCUMENT
                )
        );

    }


}
