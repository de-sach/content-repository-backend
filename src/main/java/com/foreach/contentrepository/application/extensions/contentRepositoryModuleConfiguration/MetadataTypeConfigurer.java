package com.foreach.contentrepository.application.extensions.contentRepositoryModuleConfiguration;

import com.foreach.across.modules.contentrepository.cmis.CdsCmisTypeManager;
import org.apache.chemistry.opencmis.commons.definitions.MutableTypeDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.Cardinality;
import org.apache.chemistry.opencmis.commons.enums.PropertyType;
import org.apache.chemistry.opencmis.commons.enums.Updatability;
import org.apache.chemistry.opencmis.server.support.TypeDefinitionFactory;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MetadataTypeConfigurer {
    private CdsCmisTypeManager typeManager;

    public void registerType(CdsCmisTypeManager typeManager) {
        this.typeManager = typeManager;
        if (typeManager != null) {
            TypeDefinitionFactory typeDefinitionFactory = typeManager.getTypeDefinitionFactory();
            registerMetadataDocumentTypes(typeDefinitionFactory);
        } else {
            Logger.getAnonymousLogger().log(Level.INFO, "no typemanager found");
        }
    }

    private void registerMetadataDocumentTypes(TypeDefinitionFactory typeDefinitionFactory) {
        TypeDefinition documentType = typeManager.getInternalTypeDefinition(BaseTypeId.CMIS_DOCUMENT.value());

        MutableTypeDefinition contentPackageDocumentType =
                typeDefinitionFactory.createChildTypeDefinition(documentType,
                        MetadataTypeId.TYPE_DOCUMENT);
        contentPackageDocumentType.setLocalName("Metadata Document");
        contentPackageDocumentType.setDisplayName("Metadata Document");
        contentPackageDocumentType.setDescription("Metadata Document: a document containing metadata");
        contentPackageDocumentType.setIsQueryable(false);
        contentPackageDocumentType.setIsIncludedInSupertypeQuery(false);

        addContentPackageProperty(typeDefinitionFactory, contentPackageDocumentType);

        typeManager.register(contentPackageDocumentType);
    }

    private void addContentPackageProperty(TypeDefinitionFactory typeDefinitionFactory, MutableTypeDefinition type) {
        type.addPropertyDefinition(
                typeDefinitionFactory.createPropertyDefinition(
                        MetadataTypeId.PROPERTY_METADATA_ID, "Content Package Id",
                        "Id of the Content Package object",
                        PropertyType.ID,
                        Cardinality.SINGLE,
                        Updatability.READONLY, false, false, false, false
                )
        );
    }

}
