package com.foreach.contentrepository.application.extensions.contentRepositoryModuleConfiguration;

public interface MetadataTypeId {
        String TYPE_PACKAGE = "ps:metadata";
    String TYPE_DOCUMENT = "ps:metadata_document";
//    String TYPE_FOLDER = "ps:metadata_folder";
//    String TYPE_RELATIONSHIP = "ps:metadata_relationship";

    /**
     * Id of the content package a document or folder belongs to.
     */
    String PROPERTY_METADATA_ID = "ps:contentMetadataId";

    /**
     * Id of the folder that contains the specific version.
     */
//    String PROPERTY_VERSION_FOLDER_ID = "ps:metadataVersionFolderId";

    /**
     * Id of the folder that contains all the version specific folders.
     */
//    String PROPERTY_PACKAGE_FOLDER_ID = "ps:metadataRootFolderId";

    /**
     * ObjectId of the default resource in the content package.
     */
    String PROPERTY_DEFAULT_RESOURCE = "ps:metadataDefaultResourceId";
}
