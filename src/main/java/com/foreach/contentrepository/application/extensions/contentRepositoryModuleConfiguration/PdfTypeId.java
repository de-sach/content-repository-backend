package com.foreach.contentrepository.application.extensions.contentRepositoryModuleConfiguration;

public interface PdfTypeId {
    String TYPE_PACKAGE = "ps:pdf";
    String TYPE_DOCUMENT = "ps:pdf_document";
    String KEYWORDS = "ps:pdf_keywords";

    /**
     * Id of the content package a document or folder belongs to.
     */
    String PROPERTY_PDF_ID = "ps:pdfDocumentId";
}
