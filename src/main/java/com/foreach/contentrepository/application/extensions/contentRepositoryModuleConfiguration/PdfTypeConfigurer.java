package com.foreach.contentrepository.application.extensions.contentRepositoryModuleConfiguration;

import com.foreach.across.modules.contentrepository.cmis.CdsCmisTypeManager;
import com.foreach.across.modules.contentrepository.cmis.ContentStreamHandler;
import com.foreach.across.modules.contentrepository.cmis.PropertiesHandler;
import com.foreach.across.modules.contentrepository.cmis.StrategyHandlerRegistry;
import com.foreach.across.modules.contentrepository.services.CdsFileManager;
import com.foreach.across.modules.contentrepository.services.CdsObjectStreamService;
import com.foreach.contentrepository.application.business.types.pdf.PdfContentStreamHandler;
import com.foreach.contentrepository.application.business.types.pdf.PdfPropertiesHandler;
import org.apache.chemistry.opencmis.commons.definitions.MutableTypeDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinition;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.Cardinality;
import org.apache.chemistry.opencmis.commons.enums.PropertyType;
import org.apache.chemistry.opencmis.commons.enums.Updatability;
import org.apache.chemistry.opencmis.server.support.TypeDefinitionFactory;
import org.springframework.context.annotation.Bean;

import java.util.logging.Level;
import java.util.logging.Logger;

public class PdfTypeConfigurer {

    private CdsCmisTypeManager typeManager;


    private StrategyHandlerRegistry<PropertiesHandler> propertiesHandlers;

    private StrategyHandlerRegistry<ContentStreamHandler> contentStreamHandlers;

    private CdsObjectStreamService cdsObjectStreamService;

    private CdsFileManager cdsFileManager;

    public void registerType(CdsCmisTypeManager typeManager, StrategyHandlerRegistry<PropertiesHandler> propertiesHandlers, StrategyHandlerRegistry<ContentStreamHandler> contentStreamHandlers, CdsObjectStreamService cdsObjectStreamService, CdsFileManager cdsFileManager) {
        this.cdsObjectStreamService = cdsObjectStreamService;
        this.typeManager = typeManager;
        this.cdsFileManager = cdsFileManager;
        this.propertiesHandlers = propertiesHandlers;
        this.contentStreamHandlers = contentStreamHandlers;
        if (typeManager != null) {
            TypeDefinitionFactory typeDefinitionFactory = typeManager.getTypeDefinitionFactory();
            registerPdfDocumentTypes(typeDefinitionFactory);
            registerHandlers();
        } else {
            Logger.getAnonymousLogger().log(Level.INFO, "typeManager not found");
        }
    }

    private void registerHandlers() {
        assert (propertiesHandlers != null);
        propertiesHandlers.register(PdfTypeId.TYPE_DOCUMENT, pdfPropertiesHandler(this.cdsObjectStreamService, this.cdsFileManager));
        contentStreamHandlers.register(PdfTypeId.TYPE_DOCUMENT, pdfContentStreamHandler(this.cdsObjectStreamService, this.cdsFileManager));
//        propertiesHandlers.register( BaseTypeId.CMIS_DOCUMENT.value(), documentPropertiesHandler() );
//        actionsHandlers.register( BaseTypeId.CMIS_DOCUMENT.value(), documentAllowableActionsHandler() );
//        pathHandlers.register( BaseTypeId.CMIS_DOCUMENT.value(), defaultPathHandler );
//        validationHandlers.register( BaseTypeId.CMIS_DOCUMENT.value(), defaultValidationHandler );
//        deleteHandlers.register( BaseTypeId.CMIS_DOCUMENT.value(), defaultDeleteHandler );
    }

    @Bean
    public PdfPropertiesHandler pdfPropertiesHandler(CdsObjectStreamService cdsObjectStreamService, CdsFileManager cdsFileManager) {
        return new PdfPropertiesHandler(cdsObjectStreamService, cdsFileManager);

    }

    @Bean
    public PdfContentStreamHandler pdfContentStreamHandler(CdsObjectStreamService cdsObjectStreamService, CdsFileManager cdsFileManager) {
        return new PdfContentStreamHandler(cdsObjectStreamService, cdsFileManager);
    }

    private void registerPdfDocumentTypes(TypeDefinitionFactory typeDefinitionFactory) {
        TypeDefinition documentType = typeManager.getInternalTypeDefinition(BaseTypeId.CMIS_DOCUMENT.value());

        MutableTypeDefinition pdfDocumentType =
                typeDefinitionFactory.createChildTypeDefinition(documentType,
                        PdfTypeId.TYPE_DOCUMENT);
        pdfDocumentType.setLocalName("Pdf Document");
        pdfDocumentType.setDisplayName("Pdf Document");
        pdfDocumentType.setDescription("Pdf Document: a document containing Pdf");
        pdfDocumentType.setIsQueryable(true);
        pdfDocumentType.setIsIncludedInSupertypeQuery(true);

        addPdfProperty(typeDefinitionFactory, pdfDocumentType);

        typeManager.register(pdfDocumentType);
    }

    private void addPdfProperty(TypeDefinitionFactory typeDefinitionFactory, MutableTypeDefinition type) {
        type.addPropertyDefinition(
                typeDefinitionFactory.createPropertyDefinition(
                        PdfTypeId.PROPERTY_PDF_ID, "Pdf ID",
                        "Id of the Pdf",
                        PropertyType.ID,
                        Cardinality.SINGLE,
                        Updatability.READONLY, false, false, false, false
                )
        );
        type.addPropertyDefinition(
                typeDefinitionFactory.createPropertyDefinition(
                        PdfTypeId.KEYWORDS, "PDF Keywords",
                        "keywords of the pdf after analysis of its data.",
                        PropertyType.STRING,
                        Cardinality.MULTI,
                        Updatability.READWRITE, false, false, true, true
                )
        );
    }
}
